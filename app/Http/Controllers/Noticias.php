<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Noticia;

use Storage;

class Noticias extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function mostrar()
    {
        //
        $noticias = Noticia::all();
        return view('welcome')->with(['noticias' => $noticias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
          'titulo' => 'required',
          'descripcion' => 'required'
        ]);

        $noticia = new Noticia();
        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;

        $img = $request->file('url_imagen');
        $ruta_imagen = time().'_'.$img->getClientOriginalName();
        Storage::disk('imagenes_blog')->put($ruta_imagen, file_get_contents($img->getRealPath()));

        $noticia->url_imagen = $ruta_imagen;

        if($noticia->save()){
            return back()->with('msj', 'Datos Guardados con Exito');
        }else{
            return back()->with('msj_error', 'Ocurrio un error al intentar guardar los datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = Noticia::find($id);
        return view('home')->with(['edit' => true, 'noticia' => $noticia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
          'titulo' => 'required',
          'descripcion' => 'required'
        ]);

        $noticia = Noticia::find($id);
        $noticia->titulo = $request->titulo;
        $noticia->descripcion = $request->descripcion;

        $img = $request->file('url_imagen');
        $ruta_imagen = time().'_'.$img->getClientOriginalName();
        Storage::disk('imagenes_blog')->put($ruta_imagen, file_get_contents($img->getRealPath()));
        //eliminando imagen actual
        Storage::disk('imagenes_blog')->delete($request->ing_actual);
        $noticia->url_imagen = $ruta_imagen;

        if($noticia->save()){
            return redirect('home');;
        }else{
            return back()->with('msj_error', 'Ocurrio un error al intentar actualizar los datos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Noticia::destroy($id);
        return redirect('home');
    }
}
