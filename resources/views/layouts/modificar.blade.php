@if(session()->has('msj'))
    <div class="alert alert-success" role="alert">{{ session('msj') }}</div>
@endif
@if(session()->has('msj_error'))
    <div class="alert alert-danger" role="alert">Ocurrio un error al guardar los datos, reintente mas tarde.</div>
@endif

<form class="form-horizontal" role="form" method="POST" action="{{ route('noticias.update',$noticia) }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="ing_actual" value="{{ $noticia->url_imagen }}">

    <div class="form-group">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="titulo" class="col-sm-2 control-label">Título</label>
        <div class="col-sm-10">
            <input type="text" class='form-control' name="titulo" value="{{ $noticia->titulo }}">
            @if($errors->has('titulo'))
                <span style="color:red;">{{ $errors->first('titulo') }}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
        <div class="col-sm-10">
            <textarea type="text" class='form-control' name="descripcion" > {{ $noticia->descripcion }} </textarea>
            @if($errors->has('descripcion'))
                <span style="color:red;">{{ $errors->first('descripcion') }}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="url_imagen" class="col-sm-2 control-label">Imágen</label>
        <div class="col-sm-10">
            <input type="file" class='form-control' name="url_imagen">
        </div>
    </div>
    <div class="form-grouup">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-warning">Modificar Articulo</button>
        </div>
    </div>
</form>
