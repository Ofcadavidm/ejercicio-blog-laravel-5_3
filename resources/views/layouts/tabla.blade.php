<table class="table table-hover">
    @if(isset($noticias))
        <thead>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Imagen</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            @foreach($noticias as $n)
            <tr>
                <td> {{ $n->titulo }} </td>
                <td> {{ $n->descripcion }} </td>
                <td>
                    <img src="imagenes_blog/{{$n->url_imagen}}" class ="img-responsive" alt="imagen responsiva" style="max-width:100px;"/>
                </td>
                <td>
                    <a href="noticias/{{ $n->id }}/edit" class="btn btn-warning" >Modificar</a>
                    <form action="{{ route('noticias.destroy', $n->id  ) }}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Eliminar">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    @endif
</table>
